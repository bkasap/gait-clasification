import pandas as p
import numpy as np
import pylab as plt
from scipy.stats.mstats import zscore
from scipy import interpolate
from sklearn.model_selection import cross_val_score
from sklearn import svm
from sklearn.decomposition import PCA

from dtw import dtw

def niceplots():
    '''
    update rcParams for nicer figures
    '''
    #set plot attributes
    fig_width = 5  # width in inches
    fig_height = 3  # height in inches
    fig_size =  [fig_width,fig_height]

    fig_width_pt = 300                      # Get this from LaTeX using \showthe\columnwidth
    inches_per_pt = 1.0/96                  # Convert pt to inch
    golden_mean = (sqrt(5)-1.0)/2.0         # Aesthetic ratio
    fig_width = fig_width_pt*inches_per_pt  # width in inches
    fig_height = fig_width*golden_mean      # height in inches
    #fig_height = fig_width*0.35            # height in inches
    fig_size =  [fig_width,fig_height]

    params = {'backend': 'Agg',
              'axes.labelsize': 6.5,
              'axes.titlesize': 6.5,
              'lines.linewidth' : 0.5,
              'font.size': 5.5,
              'lines.markersize': 2.,
              'lines.markeredgewidth'  : 0.1,
              'xtick.labelsize': 5.5,
              'ytick.labelsize': 5.5,
              'xtick.direction': 'in',
              'ytick.direction': 'in',
              'figure.figsize': fig_size,
              'savefig.dpi' : 600,
              'font.family': 'sans-serif',
              'axes.linewidth' : 0.5,
              'xtick.major.size' : 1.5,
              'ytick.major.size' : 1.5,
              'font.size' : 5,
              'figure.autolayout' : False,
              'legend.fontsize': 4,
              #'legend.linewidth': 1,
              'legend.numpoints': 1,
              'legend.fancybox': True}
    rcParams.update(params)

def simpleax(ax):
    '''
    Clean top and right spines of the axis
    '''
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    
def noax(ax):
    '''
    Clean top, right and bottom spines of the axis
    '''    
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.get_yaxis().tick_left()
    ax.get_xaxis().set_ticklabels([])
    ax.get_xaxis().set_ticks([])
    
def accelerationplot(walk1):
    '''
    Plots normalized accelaration on three axes (needs 0 as time axis)
    '''
    subplot(311)
    plot(walk1[:,0], zscore(walk1[:,1]))
    noax(gca())
    #xlim(x1, x2)
    ylim(-4, 4)
    #ylabel('x')
    gca().set_yticks([-3, 0, 3])
    
    subplot(312)
    plot(walk1[:,0], zscore(walk1[:,2]))
    #xlim(x1, x2)
    ylim(-4, 4)
    noax(gca())
    #ylabel('y')
    gca().set_yticks([-3, 0, 3])
    
    subplot(313)
    plot(walk1[:,0], zscore(walk1[:,3]))
    #xlim(x1, x2)
    ylim(-4, 4)
    simpleax(gca())
    #ylabel('z')
    gca().set_yticks([-3, 0, 3])
    
    xlabel('Time (s)')
    figtext(0, 0.8, 'Acceleration (normalized)', rotation='vertical')
    
def idxsfortime(walk, start_t=10, end_t=20):
    '''
    Get a time window from the whole data with time stamps
    '''
    startid = np.where(walk[:,0]>start_t)[0][0]
    endid = np.where(walk[:,0]>end_t)[0][0]
    return range(startid, endid)

def interp_norm(walk):
    '''
    Interpolate and normalize (z-score) the data
    '''
    new_walk = np.ndarray(walk.shape)
    new_walk[:,0] = np.linspace(walk[0,0], walk[-1,0], len(walk[:,0]))
    f = interpolate.interp1d(walk[:,0], walk[:,1], kind='slinear')
    new_walk[:,1] = zscore(f(walk[:,0]))
    f = interpolate.interp1d(walk[:,0], walk[:,2], kind='slinear')
    new_walk[:,2] = zscore(f(walk[:,0]))
    f = interpolate.interp1d(walk[:,0], walk[:,3], kind='slinear')
    new_walk[:,3] = zscore(f(walk[:,0]))
    return new_walk

def dynamictimewarp(x, y):
    '''
    Dynamic time warp
    Distance metric between two signals
    '''
    dist = np.zeros((len(x), len(y)))
    for i in xrange(dist.shape[0]):
        for j in xrange(dist.shape[1]):
            #dist[i, j] = sum((x[i]-y[j])**2)
            dist[i, j] = sum((x[i,:]-y[j,:])**2)
            #dist[i,j] = np.dot(x[i], y[j])
    
    cost = np.zeros((len(x), len(y)))
    cost[-1,:] = float('inf')
    cost[:,-1] = float('inf')
    cost[-1,-1] = 0
    path = [(0, 0)]
    for i in xrange(cost.shape[0]):
        for j in xrange(cost.shape[1]):
            cost[i, j] = dist[i, j]+min((cost[i-1,j], cost[i-1,j-1], cost[i,j-1]))
    
    return cost[-1, -1], cost, trackpath(cost)

def trackpath(cost):
    '''
    Finds the path on dynamictimewarp
    '''
    i, j = array(cost.shape) - 1
    p, q = [i], [j]
    while (i > 0 and j > 0):
        tb = argmin((cost[i-1, j-1], cost[i-1, j], cost[i, j-1]))
        cases = {0: (i-1, j-1),
                1: (i-1, j),
                2: (i, j-1)}

        i, j = cases[tb]
        p.insert(0, i)
        q.insert(0, j)

    p.insert(0, 0)
    q.insert(0, 0)
    return (array(p), array(q))

def scoreSVC(features, labels):
    '''
    Prints 10-fold cross validation accuracy score with svm classifier
    corrvect: (n_sample, n_features)
    labels: (n_sample)
    '''
    clf = svm.SVC(kernel='linear', C=5)
    folds = 10
    scores = cross_val_score(clf, features, labels, cv=folds)
    print labels.shape[0], ' samples ', len(np.unique(labels)), ' classes '
    print '(',folds,' folds) cross-validation '
    print 'each fold has: ', labels.shape[0]/folds, 'samples for testing ', 9*labels.shape[0]/folds, ' for training'
    print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
    print 'Scores each fold:'
    print scores
    
def datafeature(data):
    '''
    Plots the accelerometer data
    Returns correlation vectors from 3 independent time sniplets 
    '''
    # Example data x axis
    subplot(311)
    plot(data[:,1])
    # Example data y axis
    subplot(312)
    plot(data[:,2])
    # Example data z axis
    subplot(313)
    plot(data[:,3])

    print 'corr(a_x,a_y) / corr(a_x,a_z) / corr(a_y,a_z)'
    c = np.corrcoef(data[0:150,:].T)
    print [c[0,1], c[0,2], c[1,2]]
    c = np.corrcoef(data[150:300,:].T)
    print [c[0,1], c[0,2], c[1,2]]
    c = np.corrcoef(data[300:450,:].T)
    print [c[0,1], c[0,2], c[1,2]]
    c = np.corrcoef(data[450:600,:].T)
    print [c[0,1], c[0,2], c[1,2]]
    
def wholesetfeature(data):
    '''
    Plots the accelerometer data for a sample
    Returns correlation vector for the whole sequence 
    '''
    # Example data x axis
    subplot(311)
    plot(data[:,0])
    noax(gca())
    ylim(-4, 4)
    gca().set_yticks([-3, 0, 3])
    ylabel('x')
    # Example data y axis
    subplot(312)
    plot(data[:,1])
    noax(gca())
    ylim(-4, 4)
    gca().set_yticks([-3, 0, 3])
    ylabel('y')
    # Example data z axis
    subplot(313)
    plot(data[:,2])
    simpleax(gca())
    ylim(-4, 4)
    gca().set_yticks([-3, 0, 3])
    ylabel('z')
    xlabel('Time points')

    print 'corr(a_x,a_y) / corr(a_x,a_z) / corr(a_y,a_z)'
    c = np.corrcoef(data.T)
    print [c[0,1], c[0,2], c[1,2]]


def slidingwindow():

    # Handpick the walking data
    # Time-windows in the data during stable walking.
    walks = {}
    windows = [30, 80, 10, 50, 5, 40, 80, 60, 50, 20, 40, 40, 40, 60, 40, 25, 150, 250, 5, 100, 25, 50]
    for i in xrange(22):
        data = p.read_csv('data/'+str(i+1)+'.csv')
        walk = data.values
        walks[i+1] = walk[idxsfortime(walk, windows[i], windows[i]+20),:]

    # Interpolate and normalize
    # z-score within the given window. interpolation (is it needed?)

    data = {}
    for key in walks.keys():
        data[key] = interp_norm(walks[key])
        #print key, len(data[key])

    # Generate_sets
    # with a sliding_window to generate samples

    # Here we have some hyperparameters, paying attention on:
    # klen: the size of time window
    # stride: time shift on the series
    klen = 150
    stride = 10

    # perperson: the number of samples per class (depends on how much we can harvest from the data)
    # labels: the class id a sample belong to (it is ordered in the beginning but crossvalidation will scramble)
    perperson = 45 #up to perperson*stride+klen<min(len(data[i]))
    wholeset = np.ndarray((perperson*22, klen, 3))
    labels = np.zeros(perperson*22)

    for j, key in enumerate(data.keys()):
        for i in xrange(perperson):
            wholeset[perperson*j+i] = data[key][i*stride:i*stride+klen,1:4]
            labels[perperson*j+i] = key
                
    print wholeset.shape

    corrvect = np.ndarray((len(wholeset), 3))
    for i in xrange(len(wholeset)):
        c = np.corrcoef(wholeset[i].T)
        corrvect[i] = [c[0,1], c[0,2], c[1,2]]
        
    print corrvect.shape

    scoreSVC(corrvect, labels)
    return wholeset, corrvect, labels

def nooverlap():
    # Handpick the walking data
    # Time-windows in the data during stable walking.
    walks = {}

    '''
    sniplets of clean walking data to pick from
    rawdatafile / (start, end time (first column in csv)) / clean walk duration
    1 (30,130) 100
    4 (25, 90) 65
    6 (20, 140) 120
    9 (25, 100) 75
    11 (20, 160) 140 
    12 (30, 90) 60
    13 (40, 120) 80
    20 (10, 120) 110
    22 (10, 120) 110
    '''

    files = [1, 6, 11, 20, 22]
    windows = [(30,130),(20, 140),(20, 160),(10, 120),(10, 120)]

    for i, f in enumerate(files):
        data = p.read_csv('data/'+str(f)+'.csv')
        walk = data.values
        walks[f] = walk[idxsfortime(walk, windows[i][0], windows[i][1]),:]

    # Interpolate and normalize
    # z-score within the given window. interpolation (is it needed?)

    data = {}
    for key in walks.keys():
        data[key] = interp_norm(walks[key])
    # Generate_sets
    # with a sliding_window to generate samples
    # In case of no overlaps, stride >= klen

    # Here we have some hyperparameters, paying attention on:
    # klen: the size of time window
    # stride: time shift on the series
    klen = 100
    stride = 100

    # perperson: the number of samples per class (depends on how much we can harvest from the data)
    # labels: the class id a sample belong to (it is ordered in the beginning but crossvalidation will scramble)
    perperson = 33 #up to perperson*stride+klen<600
    wholeset = np.ndarray((perperson*len(data.keys()), klen, 3))
    labels = np.zeros(perperson*len(data.keys()))

    for j, key in enumerate(data.keys()):
        for i in xrange(perperson):
            wholeset[perperson*j+i] = data[key][i*stride:i*stride+klen,1:4]
            labels[perperson*j+i] = key
                
    print wholeset.shape

    corrvect = np.ndarray((len(wholeset), 3))
    for i in xrange(len(wholeset)):
        c = np.corrcoef(wholeset[i].T)
        corrvect[i] = [c[0,1], c[0,2], c[1,2]]
        
    print corrvect.shape

    scoreSVC(corrvect, labels)

    return wholeset, corrvect, labels

def PCAplots(corrvect, labels):
    # PCA with 3 components
    # samples from 4 classes are highlighted (by color)

    pca = PCA(n_components=3)
    X_pca = pca.fit_transform(corrvect)

    # How much variance each principal component benefits
    print 'PCA explained variance ratio by component [1st, 2nd, 3rd]: '
    print pca.explained_variance_ratio_

    plt.figure()
    # plot the data projected on first two principal components
    plt.plot(X_pca[:, 0], X_pca[:, 1], "ko", markersize=1)
    plt.plot(X_pca[labels==1, 0], X_pca[labels==1, 1], "ro")
    plt.plot(X_pca[labels==11, 0], X_pca[labels==11, 1], "bo")
    plt.plot(X_pca[labels==20, 0], X_pca[labels==20, 1], "go")
    plt.plot(X_pca[labels==15, 0], X_pca[labels==15, 1], "ko")
    plt.title("Projection by PCA")
    plt.xlabel("1st principal component")
    plt.ylabel("2nd component")
    plt.tight_layout()
    #savefig('pca_12_overlapping.pdf')

    plt.figure()
    plt.plot(X_pca[:, 0], X_pca[:, 2], "ko", markersize=1)
    plt.plot(X_pca[labels==1, 0], X_pca[labels==1, 2], "ro")
    plt.plot(X_pca[labels==11, 0], X_pca[labels==11, 2], "bo")
    plt.plot(X_pca[labels==20, 0], X_pca[labels==20, 2], "go")
    plt.plot(X_pca[labels==15, 0], X_pca[labels==15, 2], "ko")
    plt.title("Projection by PCA")
    plt.xlabel("1st principal component")
    plt.ylabel("3rd component")
    plt.tight_layout()
    plt.show()
    #savefig('pca_13_overlapping.pdf')

def main():
    print "Results: sliding window "
    wholeset, corrvect, labels = slidingwindow()
    PCAplots(corrvect, labels)
    print "Results: no-overlap "
    wholeset, corrvect, labels = nooverlap()
    PCAplots(corrvect, labels)


if __name__ == "__main__":
    main()